- Add debug: `Tools/SuperCloner/Unhide Hidden Clones` (for unhiding hidden clones).

### 0.1.1
- LinearCloner: keeping constant spacing between modes (IterationMode).
  How? Any `Operator` now may override `OnChange()` and return a callback that will be executed after the change.

### 0.1.0
